/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QDoubleSpinBox *linear_a_SpinBox;
    QLabel *label_3;
    QDoubleSpinBox *linear_b_SpinBox;
    QPushButton *linear_pushButton;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_5;
    QDoubleSpinBox *sqr_a_SpinBox;
    QLabel *label_6;
    QDoubleSpinBox *sqr_b_SpinBox;
    QLabel *label_7;
    QDoubleSpinBox *sqr_c_SpinBox;
    QPushButton *sqr_pushButton;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_9;
    QDoubleSpinBox *cube_a_SpinBox;
    QLabel *label_11;
    QDoubleSpinBox *cube_b_SpinBox;
    QLabel *label_12;
    QDoubleSpinBox *cube_c_SpinBox;
    QLabel *label_10;
    QDoubleSpinBox *cube_d_SpinBox;
    QPushButton *cube_pushButton;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_13;
    QRadioButton *randA_radioButton;
    QRadioButton *selfA_radioButton;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_14;
    QDoubleSpinBox *arithm_a_SpinBox;
    QLabel *label_15;
    QDoubleSpinBox *arithm_d_SpinBox;
    QLabel *label_16;
    QSpinBox *arithm_n_spinBox;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *arithm_elem_pushButton;
    QPushButton *arithm_sum_pushButton;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_17;
    QRadioButton *randG_radioButton;
    QRadioButton *selfG_radioButton;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_18;
    QDoubleSpinBox *geom_b_SpinBox;
    QLabel *label_19;
    QDoubleSpinBox *geom_q_SpinBox;
    QLabel *label_20;
    QSpinBox *geom_n_spinBox;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *geom_elem_pushButton;
    QPushButton *geom_sum_pushButton;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_21;
    QSpinBox *factor_n_spinBox;
    QPushButton *factorial_pushButton;
    QTextEdit *textEdit;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(875, 667);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 20, 847, 625));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(10);
        label->setFont(font);

        verticalLayout->addWidget(label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setPointSize(9);
        label_2->setFont(font1);

        horizontalLayout->addWidget(label_2);

        linear_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        linear_a_SpinBox->setObjectName(QString::fromUtf8("linear_a_SpinBox"));
        linear_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout->addWidget(linear_a_SpinBox);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        horizontalLayout->addWidget(label_3);

        linear_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        linear_b_SpinBox->setObjectName(QString::fromUtf8("linear_b_SpinBox"));
        linear_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout->addWidget(linear_b_SpinBox);

        linear_pushButton = new QPushButton(layoutWidget);
        linear_pushButton->setObjectName(QString::fromUtf8("linear_pushButton"));
        linear_pushButton->setFont(font1);

        horizontalLayout->addWidget(linear_pushButton);


        verticalLayout->addLayout(horizontalLayout);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        verticalLayout->addWidget(label_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font1);

        horizontalLayout_2->addWidget(label_5);

        sqr_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        sqr_a_SpinBox->setObjectName(QString::fromUtf8("sqr_a_SpinBox"));
        sqr_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_2->addWidget(sqr_a_SpinBox);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font1);

        horizontalLayout_2->addWidget(label_6);

        sqr_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        sqr_b_SpinBox->setObjectName(QString::fromUtf8("sqr_b_SpinBox"));
        sqr_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_2->addWidget(sqr_b_SpinBox);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font1);

        horizontalLayout_2->addWidget(label_7);

        sqr_c_SpinBox = new QDoubleSpinBox(layoutWidget);
        sqr_c_SpinBox->setObjectName(QString::fromUtf8("sqr_c_SpinBox"));
        sqr_c_SpinBox->setMinimum(-99.000000000000000);

        horizontalLayout_2->addWidget(sqr_c_SpinBox);

        sqr_pushButton = new QPushButton(layoutWidget);
        sqr_pushButton->setObjectName(QString::fromUtf8("sqr_pushButton"));
        sqr_pushButton->setFont(font1);

        horizontalLayout_2->addWidget(sqr_pushButton);


        verticalLayout->addLayout(horizontalLayout_2);

        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font);

        verticalLayout->addWidget(label_8);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font1);

        horizontalLayout_3->addWidget(label_9);

        cube_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_a_SpinBox->setObjectName(QString::fromUtf8("cube_a_SpinBox"));
        cube_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_3->addWidget(cube_a_SpinBox);

        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font1);

        horizontalLayout_3->addWidget(label_11);

        cube_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_b_SpinBox->setObjectName(QString::fromUtf8("cube_b_SpinBox"));
        cube_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_3->addWidget(cube_b_SpinBox);

        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setFont(font1);

        horizontalLayout_3->addWidget(label_12);

        cube_c_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_c_SpinBox->setObjectName(QString::fromUtf8("cube_c_SpinBox"));
        cube_c_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_3->addWidget(cube_c_SpinBox);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font1);

        horizontalLayout_3->addWidget(label_10);

        cube_d_SpinBox = new QDoubleSpinBox(layoutWidget);
        cube_d_SpinBox->setObjectName(QString::fromUtf8("cube_d_SpinBox"));
        cube_d_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_3->addWidget(cube_d_SpinBox);

        cube_pushButton = new QPushButton(layoutWidget);
        cube_pushButton->setObjectName(QString::fromUtf8("cube_pushButton"));
        cube_pushButton->setFont(font1);

        horizontalLayout_3->addWidget(cube_pushButton);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_13 = new QLabel(layoutWidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setFont(font);

        horizontalLayout_4->addWidget(label_13);

        randA_radioButton = new QRadioButton(layoutWidget);
        randA_radioButton->setObjectName(QString::fromUtf8("randA_radioButton"));
        randA_radioButton->setFont(font1);
        randA_radioButton->setChecked(false);

        horizontalLayout_4->addWidget(randA_radioButton);

        selfA_radioButton = new QRadioButton(layoutWidget);
        selfA_radioButton->setObjectName(QString::fromUtf8("selfA_radioButton"));
        selfA_radioButton->setFont(font1);
        selfA_radioButton->setChecked(true);

        horizontalLayout_4->addWidget(selfA_radioButton);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_14 = new QLabel(layoutWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font1);

        horizontalLayout_5->addWidget(label_14);

        arithm_a_SpinBox = new QDoubleSpinBox(layoutWidget);
        arithm_a_SpinBox->setObjectName(QString::fromUtf8("arithm_a_SpinBox"));
        arithm_a_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_5->addWidget(arithm_a_SpinBox);

        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font1);

        horizontalLayout_5->addWidget(label_15);

        arithm_d_SpinBox = new QDoubleSpinBox(layoutWidget);
        arithm_d_SpinBox->setObjectName(QString::fromUtf8("arithm_d_SpinBox"));
        arithm_d_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_5->addWidget(arithm_d_SpinBox);

        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font1);

        horizontalLayout_5->addWidget(label_16);

        arithm_n_spinBox = new QSpinBox(layoutWidget);
        arithm_n_spinBox->setObjectName(QString::fromUtf8("arithm_n_spinBox"));
        arithm_n_spinBox->setMinimum(1);

        horizontalLayout_5->addWidget(arithm_n_spinBox);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        arithm_elem_pushButton = new QPushButton(layoutWidget);
        arithm_elem_pushButton->setObjectName(QString::fromUtf8("arithm_elem_pushButton"));
        arithm_elem_pushButton->setFont(font1);

        horizontalLayout_6->addWidget(arithm_elem_pushButton);

        arithm_sum_pushButton = new QPushButton(layoutWidget);
        arithm_sum_pushButton->setObjectName(QString::fromUtf8("arithm_sum_pushButton"));
        arithm_sum_pushButton->setFont(font1);

        horizontalLayout_6->addWidget(arithm_sum_pushButton);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_17 = new QLabel(layoutWidget);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setFont(font);

        horizontalLayout_7->addWidget(label_17);

        randG_radioButton = new QRadioButton(layoutWidget);
        randG_radioButton->setObjectName(QString::fromUtf8("randG_radioButton"));
        randG_radioButton->setChecked(false);

        horizontalLayout_7->addWidget(randG_radioButton);

        selfG_radioButton = new QRadioButton(layoutWidget);
        selfG_radioButton->setObjectName(QString::fromUtf8("selfG_radioButton"));
        selfG_radioButton->setChecked(true);

        horizontalLayout_7->addWidget(selfG_radioButton);


        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_18 = new QLabel(layoutWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setFont(font1);

        horizontalLayout_8->addWidget(label_18);

        geom_b_SpinBox = new QDoubleSpinBox(layoutWidget);
        geom_b_SpinBox->setObjectName(QString::fromUtf8("geom_b_SpinBox"));
        geom_b_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_8->addWidget(geom_b_SpinBox);

        label_19 = new QLabel(layoutWidget);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setFont(font1);

        horizontalLayout_8->addWidget(label_19);

        geom_q_SpinBox = new QDoubleSpinBox(layoutWidget);
        geom_q_SpinBox->setObjectName(QString::fromUtf8("geom_q_SpinBox"));
        geom_q_SpinBox->setMinimum(-99.989999999999995);

        horizontalLayout_8->addWidget(geom_q_SpinBox);

        label_20 = new QLabel(layoutWidget);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setFont(font1);

        horizontalLayout_8->addWidget(label_20);

        geom_n_spinBox = new QSpinBox(layoutWidget);
        geom_n_spinBox->setObjectName(QString::fromUtf8("geom_n_spinBox"));
        geom_n_spinBox->setMinimum(1);

        horizontalLayout_8->addWidget(geom_n_spinBox);


        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        geom_elem_pushButton = new QPushButton(layoutWidget);
        geom_elem_pushButton->setObjectName(QString::fromUtf8("geom_elem_pushButton"));

        horizontalLayout_9->addWidget(geom_elem_pushButton);

        geom_sum_pushButton = new QPushButton(layoutWidget);
        geom_sum_pushButton->setObjectName(QString::fromUtf8("geom_sum_pushButton"));
        geom_sum_pushButton->setFont(font1);

        horizontalLayout_9->addWidget(geom_sum_pushButton);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_21 = new QLabel(layoutWidget);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setFont(font);

        horizontalLayout_10->addWidget(label_21);

        factor_n_spinBox = new QSpinBox(layoutWidget);
        factor_n_spinBox->setObjectName(QString::fromUtf8("factor_n_spinBox"));
        factor_n_spinBox->setMaximum(9);

        horizontalLayout_10->addWidget(factor_n_spinBox);

        factorial_pushButton = new QPushButton(layoutWidget);
        factorial_pushButton->setObjectName(QString::fromUtf8("factorial_pushButton"));

        horizontalLayout_10->addWidget(factorial_pushButton);


        verticalLayout->addLayout(horizontalLayout_10);

        textEdit = new QTextEdit(layoutWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        verticalLayout->addWidget(textEdit);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "1) \320\240\320\265\321\210\320\265\320\275\320\270\320\265 \320\273\320\270\320\275\320\265\320\271\320\275\320\276\320\263\320\276 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\321\217: ax + b = 0", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        linear_pushButton->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214 \320\273\320\270\320\275\320\265\320\271\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "2) \320\240\320\265\321\210\320\265\320\275\320\270\320\265 \320\272\320\262\320\260\320\264\321\200\320\260\321\202\320\275\320\276\320\263\320\276 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\321\217: ax^2 + bx + c = 0", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 c:", nullptr));
        sqr_pushButton->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214 \320\272\320\262\320\260\320\264\321\200\320\260\321\202\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "3) \320\240\320\265\321\210\320\265\320\275\320\270\320\265 \320\272\321\203\320\261\320\270\321\207\320\265\321\201\320\272\320\276\320\263\320\276 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\321\217: ax^3 + bx^2 + cx + d = 0", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 a:", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 b:", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 c:", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 d:", nullptr));
        cube_pushButton->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214 \320\272\321\203\320\261\320\270\321\207\320\265\321\201\320\272\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "4) \320\227\320\260\320\264\320\260\321\202\321\214 \320\260\321\200\320\270\321\204\320\274\320\265\321\202\320\270\321\207\320\265\321\201\320\272\321\203\321\216 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\321\216", nullptr));
        randA_radioButton->setText(QApplication::translate("MainWindow", "\321\201\320\273\321\203\321\207\320\260\320\271\320\275\320\276", nullptr));
        selfA_radioButton->setText(QApplication::translate("MainWindow", "\320\262\321\200\321\203\321\207\320\275\321\203\321\216", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\260\321\207\320\260\320\273\321\214\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202:", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\200\320\260\320\267\320\275\320\276\321\201\321\202\321\214 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270: ", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\276\320\274\320\265\321\200 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260:", nullptr));
        arithm_elem_pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \320\262\321\213\320\261\321\200\320\260\320\275\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202", nullptr));
        arithm_sum_pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \321\201\321\203\320\274\320\274\321\203 \320\264\320\276 \320\262\321\213\320\261\321\200\320\260\320\275\320\275\320\276\320\263\320\276 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "5) \320\227\320\260\320\264\320\260\321\202\321\214 \320\263\320\265\320\276\320\274\320\265\321\202\321\200\320\270\321\207\320\265\321\201\320\272\321\203\321\216 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\321\216", nullptr));
        randG_radioButton->setText(QApplication::translate("MainWindow", "\321\201\320\273\321\203\321\207\320\260\320\271\320\275\320\276", nullptr));
        selfG_radioButton->setText(QApplication::translate("MainWindow", "\320\262\321\200\321\203\321\207\320\275\321\203\321\216", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\260\321\207\320\260\320\273\321\214\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202:", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\267\320\275\320\260\320\274\320\265\320\275\320\260\321\202\320\265\320\273\321\214 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270: ", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\275\320\276\320\274\320\265\321\200 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260:", nullptr));
        geom_elem_pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \320\262\321\213\320\261\321\200\320\260\320\275\320\275\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202", nullptr));
        geom_sum_pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \321\201\321\203\320\274\320\274\321\203 \320\262\321\201\320\265\321\205 \321\207\320\273\320\265\320\275\320\276\320\262 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270", nullptr));
        label_21->setText(QApplication::translate("MainWindow", "6) \320\222\321\213\321\207\320\270\320\273\320\270\321\202\321\214 \321\204\320\260\320\272\321\202\320\276\321\200\320\270\320\260\320\273:", nullptr));
        factorial_pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
