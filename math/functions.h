/**
*\file
*\brief
Модуль программы
*\author Арина
*\version 1.0
*
*Функции, необходимые для классический матемтаический операций
*/


#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QVector>
#include <QtMath>

/**
 * @brief linearEquation - Функция для вычисления корня линейного уравнения.
 * @param a - Коэффициент при х
 * @param b - Свободный член
 * @return Возвращает искомый корень
 * Исходный код:
 * \code
 * double linearEquation(double a, double b)
 * {
 *   if (fabs(a) < 0.000000001)
 *       return 0;
 *   else
 *       return -b/a;
 * }
 * \endcode
 */
double linearEquation(double a, double b);

/**
 * @brief squareEquation - Функция для вычисления действительных корней квадратного уравнения.
 * @param a - Коэффициент при х^2
 * @param b - Коэффициент при х
 * @param c - Свободный член
 * @return Возвращает вектор с искомыми корнями
 * Исходный код:
 * \code
 * QVector<double> squareEquation(double a, double b, double c)
 * {
 *  QVector<double> x;
 *  double D = b * b - 4 * a * c;
 *  if (D < 0)
 *      return x;
 *  else {
 *      x = {(-b+sqrt(D))/(2 * a), (-b-sqrt(D))/(2 * a)};
 *     return x;
 *  }
 * }
 * \endcode
 */
QVector<double> squareEquation(double a, double b, double c);

/**
 * @brief cubeEquation - Функция для вычисления действительных корней кубического уравнения.
 * @param a1 - Коэффициент при х^3
 * @param b1 - Коэффициент при х^2
 * @param c1 - Коэффициент при х
 * @param d1 - Свободный член
 * @return Возвращает вектор с искомыми корнями
 * Исходный код:
 * \code
 * QVector<double> cubeEquation(double a1, double b1, double c1, double d1)
 *{
 *  QVector<double> x;
 *  double a = b1/a1, b = c1/a1, c = d1/a1;
 *  double q,r,r2,q3;
 *    q=(a*a-3.*b)/9.; r=(a*(2.*a*a-9.*b)+27.*c)/54.;
 *    r2=r*r; q3=q*q*q;
 *    if(r2<q3) {
 *      double t=acos(r/sqrt(q3))/3.;
 *      x.push_back(-2.*sqrt(q)*cos(t)-a/3.);
 *      x.push_back(-2.*sqrt(q)*cos(t+2*M_PI/3.)-a/3.);
 *      x.push_back(-2.*sqrt(q)*cos(t-2*M_PI/3.)-a/3.);
 *    }
 *    else {
 *      double aa,bb;
 *      aa=-pow(fabs(r)+sqrt(r2-q3),1./3.);
 *      if (r < 0)
 *          aa = -aa;
 *      if (fabs(aa) > 0.000000001)
 *        bb=q/aa;
 *      else
 *          bb=0.;
 *     x.push_back(aa+bb-a/3);
 *      if (fabs(aa-bb) < 0.000000001)
 *          x.push_back(-aa-a/3);
 *    }
 *    return x;
 *}
 * \endcode
 */
QVector<double> cubeEquation(double a1, double b1, double c1, double d1);

/**
 * @brief elemArithProgress - Функция для вычисления n-го элемента арифметической прогрессии.
 * @param n - Номер искомого элемента
 * @param a - Первый элемент прогрессии
 * @param d - Разность прогрессии
 * @return Возвращает искомый элемент
 * Исходный код:
 * \code
 * double elemArithProgress(int n, double a, double d)
 *{
 *   return a + (n-1) * d;
 *}
 * \endcode
 */
double elemArithProgress(int n, double a, double d);

/**
 * @brief sumArithProgress - Функция для вычисления суммы арифметической прогрессии.
 * @param n - Количество членов в прогрессии
 * @param a - Первый элемент прогрессии
 * @param d - Разность прогрессии
 * @return Возвращает искомую сумму
 * Исходный код:
 * \code
 * double sumArithProgress(int n, double a, double d)
 *{
 *  return ((2 * a + (n-1) * d) * n) / 2;
 *}
 * \endcode
 */
double sumArithProgress(int n, double a, double d);

/**
 * @brief sumGeomProgress - Функция для вычисления суммы геометрической прогрессии.
 * @param n - Количество членов в прогрессии
 * @param b - Первый элемент прогрессии
 * @param q - Знаменатель прогрессии
 * @return Возвращает искомую сумму
 * Исходный код:
 * \code
 * double sumGeomProgress(int n, double b, double q)
 *{
 *  return b * (1 - pow(q, n)) / (1 - q);
 *}
 * \endcode
 */
double sumGeomProgress(int n, double b, double q);

/**
 * @brief elemGeomProgress - Функция для вычисления n-го элемента геометрической прогрессии.
 * @param n - Номер искомого элемента
 * @param b - Первый элемент прогрессии
 * @param q - Знаменатель прогрессии
 * @return Возвращает искомый элемент
 * Исходный код:
 * \code
 * double elemGeomProgress(int n, double b, double q)
 *{
 *  return b * pow(q, n-1);
 *}
 * \endcode
 */
double elemGeomProgress(int n, double b, double q);

/**
 * @brief factorial - Функция для вычисления факториала числа.
 * @param n - Заданное число
 * @return Возвращает факториал заданного числа
 * Исходный код:
 * \code
 * double factorial (int n)
 *{
 *  int addit = 1;
 *  int fact = 1;
 *  if(n == 0)
 *      return 1;
 *  else
 *  {
 *      while(addit != n+1)
 *      {
 *          fact *= addit;
 *          addit++;
 *      }
 *      return fact;
 *  }
 *}
 * \endcode
 */
double factorial (int n);


#endif // FUNCTIONS_H
