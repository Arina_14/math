#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    srand( time(nullptr));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_linear_pushButton_clicked()
{
    double a, b;
    a = ui->linear_a_SpinBox->value();
    b = ui->linear_b_SpinBox->value();
    if (fabs(a) < 0.000000001)
        ui->textEdit->insertPlainText("Неверно заданное линейное уравнение!\n");
    else {
        double s = linearEquation(a, b);
        ui->textEdit->insertPlainText("Корень линейного уравнения = " + QString::number(s) + "\n");
    }
}

void MainWindow::on_sqr_pushButton_clicked()
{
    double a, b, c;
    a = ui->sqr_a_SpinBox->value();
    b = ui->sqr_b_SpinBox->value();
    c = ui->sqr_c_SpinBox->value();
    if (fabs(a) < 0.000000001)
        ui->textEdit->insertPlainText("Неверно заданное квадратное уравнение!\n");
    else {
        if (b * b - 4 * a * c < 0)
            ui->textEdit->insertPlainText("Отсутствуют действительные корни квадратного уравнения.\n");
        else {
            QVector<double> s = squareEquation(a, b, c);
            if(fabs(s[0] - s[1]) < 0.000000001)
                ui->textEdit->insertPlainText("Корень квадратного уравнения = " + QString::number(s[0]) + "\n");
            else
                ui->textEdit->insertPlainText("Корни квадратного уравнения: " + QString::number(s[0]) + ", "
                        + QString::number(s[1]) + "\n");
        }
    }
}

void MainWindow::on_cube_pushButton_clicked()
{
    double a, b, c, d;
    a = ui->cube_a_SpinBox->value();
    b = ui->cube_b_SpinBox->value();
    c = ui->cube_c_SpinBox->value();
    d = ui->cube_d_SpinBox->value();
    if (fabs(a) < 0.000000001)
        ui->textEdit->insertPlainText("Неверно заданное кубическое уравнение!\n");
    else {
        QVector<double> s = cubeEquation(a, b, c, d);
        if(s.size() == 1) {
            ui->textEdit->insertPlainText("Корень кубического уравнения = " + QString::number(s[0]) + "\n");
        }
        else {
            ui->textEdit->insertPlainText("Корни кубического уравнения: " + QString::number(s[0]));
            for (int i = 1; i < s.size(); ++i)
                ui->textEdit->insertPlainText(", " + QString::number(s[i]));
            ui->textEdit->insertPlainText("\n");
        }
    }
}

void MainWindow::on_arithm_elem_pushButton_clicked()
{
    double a, d;
    int n = ui->arithm_n_spinBox->value();
    if (ui->randA_radioButton->isChecked()) {
        a = rand() % 200 - 100;
        d = rand() % 200 - 100;
    }
    else {
        a = ui->arithm_a_SpinBox->value();
        d = ui->arithm_d_SpinBox->value();
    }
    double s = elemArithProgress(n, a, d);
    ui->textEdit->insertPlainText("Арифм. прогрессия(a1 = " + QString::number(a) + ", d = " + QString::number(d) + "): "
                                  + QString::number(n) + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_arithm_sum_pushButton_clicked()
{
    double a, d;
    int n = ui->arithm_n_spinBox->value();
    if (ui->randA_radioButton->isChecked()) {
        a = rand() % 200 - 100;
        d = rand() % 200 - 100;
    }
    else {
        a = ui->arithm_a_SpinBox->value();
        d = ui->arithm_d_SpinBox->value();
    }
    double s = sumArithProgress(n, a, d);
    ui->textEdit->insertPlainText("Арифм. прогрессия(a1 = " + QString::number(a) + ", d = " + QString::number(d) + "): "
                                  + "сумма по " + QString::number(n) + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_geom_sum_pushButton_clicked()
{
    double b, q;
    int n = ui->geom_n_spinBox->value();
    if (ui->randG_radioButton->isChecked()) {
        b = rand() % 200 - 100;
        q = rand() % 200 - 100;
    }
    else {
        b = ui->geom_b_SpinBox->value();
        q = ui->geom_q_SpinBox->value();
    }
    double s = sumGeomProgress(n, b, q);
    ui->textEdit->insertPlainText("Геом. прогрессия(b1 = " + QString::number(b) + ", q = "
                                  + QString::number(q) + "): " + "сумма по " + QString::number(n)
                                  + "-й элемент = " + QString::number(s) + "\n");
}

void MainWindow::on_geom_elem_pushButton_clicked()
{
    double b, q;
    int n = ui->geom_n_spinBox->value();
    if (ui->randG_radioButton->isChecked()) {
        b = rand() % 200 - 100;
        q = rand() % 200 - 100;
    }
    else {
        b = ui->geom_b_SpinBox->value();
        q = ui->geom_q_SpinBox->value();
    }
    double elem = elemGeomProgress(n, b, q);
    ui->textEdit->insertPlainText("Геом. прогрессия(b1 = " + QString::number(b) + ", q = "
                                  + QString::number(q) + "): "
                                  + QString::number(n) + "-й элемент = " + QString::number(elem) + "\n");
}

void MainWindow::on_factorial_pushButton_clicked()
{
    int n = ui->factor_n_spinBox->value();
    double fact;
    fact = factorial(n);
    ui->textEdit->insertPlainText("Факториал числа " + QString::number(n) + " = " + QString::number(fact) + "\n");
}
