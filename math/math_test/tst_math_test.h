#ifndef TST_MATH_TEST_H
#define TST_MATH_TEST_H

#include <QtCore>
#include <QtTest/QtTest>

class math_test : public QObject
{
    Q_OBJECT

public:
    math_test();

private slots:
    void testLinear();
    void testSquare();
    void testCube();
    void testElemA();
    void testSumA();
    void testSumG();
    void testElemG();
    void testFactorial();

};

#endif // TST_MATH_TEST_H
