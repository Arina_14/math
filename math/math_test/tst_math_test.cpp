#include <QtTest>
#include "../functions.cpp"
#include "tst_math_test.h"

//tests

math_test::math_test()
{
}

bool compareVec(const QVector<double> &vec1, const QVector<double> &vec2) {
    if (vec1 == vec2)
        return true;
    return false;
}

void math_test::testLinear()
{
    double a = 6, b = 12;
    QCOMPARE(-2, linearEquation(a, b));

    a = 0.5;
    b = -2.5;
    QCOMPARE(5, linearEquation(a, b));
}

void math_test::testSquare()
{
    double a = 1, b = 2, c = -3;
    QCOMPARE(true, compareVec(squareEquation(a, b, c), {1, -3}) || compareVec(squareEquation(a, b, c), {-3, 1}));

    a = 1;
    b = 0;
    c = -4;
    QCOMPARE(true, compareVec(squareEquation(a, b, c), {2, -2}) || compareVec(squareEquation(a, b, c), {-2, 2}));
}

void math_test::testCube()
{
    double a = 1, b = 1, c = 1, d = 1;
    QVector<double> s = cubeEquation(a, b, c, d);
    QCOMPARE(-1., s[0]);

    a = 1;
    b = 1;
    c = -1;
    d = -1;
    s = cubeEquation(a, b, c, d);
    QCOMPARE(1., s[0]);
}

void math_test::testElemA()
{
    int n = 3;
    double a = 2, b = 1;
    QCOMPARE(4, elemArithProgress(n, a, b));

    n = 2;
    a = 8;
    b = 12;
    QCOMPARE(20, elemArithProgress(n, a, b));
}

void math_test::testSumA()
{
    int n = 3;
    double a = 2, b = 1;
    QCOMPARE(9, sumArithProgress(n, a, b));

    n = 2;
    a = 8;
    b = 12;
    QCOMPARE(28, sumArithProgress(n, a, b));
}

void math_test::testSumG()
{
    int n = 3;
    double b = 2, q = 2;
    QCOMPARE(14, sumGeomProgress(n, b, q));

    n = 2;
    b = 8;
    q = 3;
    QCOMPARE(32, sumGeomProgress(n, b, q));
}

void math_test::testElemG()
{
    int n = 2;
    double b = 10, q = 2;
    QCOMPARE(20, elemGeomProgress(n, b, q));

    n = 5;
    b = 3;
    q = 1;
    QCOMPARE(3, elemGeomProgress(n, b, q));
}

void math_test::testFactorial()
{
    int n = 3;
    QCOMPARE(6, factorial(n));

    n = 5;
    QCOMPARE(120, factorial(n));
}
